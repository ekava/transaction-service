package com.example.transactionservice.repository;

import com.example.transactionservice.dto.TransactionResponseDto;
import com.example.transactionservice.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface TransactionRepository extends JpaRepository<Transaction, UUID> {

    List<TransactionResponseDto> findAllByClientId(UUID clientId);
}