package com.example.transactionservice.entity.enums;

public enum TransactionStatus {
    ACCEPTED,
    DECLINED,
    PROCESSING
}
