package com.example.transactionservice.entity.enums;

public enum TransactionType {
    BUY,
    SELL,
    FIX
}
