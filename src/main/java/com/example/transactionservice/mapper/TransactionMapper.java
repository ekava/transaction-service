package com.example.transactionservice.mapper;

import com.example.transactionservice.dto.TransactionRequestDto;
import com.example.transactionservice.dto.TransactionResponseDto;
import com.example.transactionservice.entity.Transaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TransactionMapper {

    @Mapping(target = "id", expression = "java(java.util.UUID.randomUUID())")
    Transaction transactionRequestDtoToTransaction(TransactionRequestDto transactionRequestDto);

    TransactionResponseDto transactionToTransactionResponseDto(Transaction transaction);
}
