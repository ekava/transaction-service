package com.example.transactionservice.exception.custom;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.server.ResponseStatusException;

public class BadRequestException extends ResponseStatusException {

    public BadRequestException(HttpStatusCode status) {
        super(status);
    }

    public BadRequestException(HttpStatusCode status, String reason) {
        super(status, reason);
    }

    public BadRequestException(String reason) {
        this(HttpStatus.BAD_REQUEST, reason);
    }

    public BadRequestException() {
        super(HttpStatus.BAD_REQUEST);
    }
}
