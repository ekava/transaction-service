package com.example.transactionservice.exception;

import java.util.List;

public record ValidationErrorResponse(String errorCode,
                                      List<ErrorExtension> errors) {
    public ValidationErrorResponse(List<ErrorExtension> errorExtensions) {
        this("validation_failed", errorExtensions);
    }
}
