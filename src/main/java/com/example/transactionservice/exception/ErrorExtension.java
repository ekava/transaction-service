package com.example.transactionservice.exception;

public record ErrorExtension(String errorMessage, String errorCode) {
    public static ErrorExtension createForInvalidQueryParam(String errorMessage) {
        return new ErrorExtension(errorMessage, "invalid_query_param");
    }
}
