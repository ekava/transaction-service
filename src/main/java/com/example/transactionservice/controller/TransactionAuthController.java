package com.example.transactionservice.controller;

import com.example.transactionservice.dto.TransactionRequestDto;
import com.example.transactionservice.dto.TransactionResponseDto;
import com.example.transactionservice.service.interfaces.TransactionService;
import jakarta.annotation.security.RolesAllowed;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth/transactions")
public class TransactionAuthController {

    private final TransactionService transactionService;
    private final RabbitTemplate rabbitTemplate;

    @RolesAllowed({"owner", "worker", "user"})
    @GetMapping
    public List<TransactionResponseDto> getAllTransactions(@AuthenticationPrincipal Jwt jwt) {
        UUID clientId = UUID.fromString(jwt.getSubject());
        return transactionService.getAllTransactions(clientId);
    }

    @RolesAllowed("owner")
    @GetMapping("/all")
    public List<TransactionResponseDto> getAll() {
        return transactionService.getAll();
    }

    @RolesAllowed({"owner", "worker"})
    @PostMapping("/buy")
    public void buyProduct(@AuthenticationPrincipal Jwt jwt,
                           @RequestBody TransactionRequestDto transactionRequestDto) {
        UUID clientId = UUID.fromString(jwt.getSubject());
        transactionService.buyProduct(clientId, transactionRequestDto);
    }

    @RolesAllowed({"owner", "worker", "user"})
    @PostMapping("/sell")
    public void sellProduct(@AuthenticationPrincipal Jwt jwt,
                            @RequestBody TransactionRequestDto transactionRequestDto) {
        UUID clientId = UUID.fromString(jwt.getSubject());
        transactionService.sellProduct(clientId, transactionRequestDto);
    }

    @RolesAllowed({"owner", "worker"})
    @PostMapping("/fix")
    public void fixProduct(@AuthenticationPrincipal Jwt jwt,
                           @RequestBody TransactionRequestDto transactionRequestDto) {
        UUID clientId = UUID.fromString(jwt.getSubject());
        transactionService.fixProduct(clientId, transactionRequestDto);
    }

    @RolesAllowed({"owner", "worker", "user"})
    @PostMapping("/hello")
    public void hello(String message) {
        rabbitTemplate.convertAndSend(message);
    }
}
