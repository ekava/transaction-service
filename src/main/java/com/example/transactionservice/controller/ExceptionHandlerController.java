package com.example.transactionservice.controller;


import com.example.transactionservice.exception.ErrorExtension;
import com.example.transactionservice.exception.ValidationErrorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  @Nullable HttpHeaders headers,
                                                                  @Nullable HttpStatusCode status,
                                                                  @Nullable WebRequest request) {
        List<ErrorExtension> errors = new ArrayList<>(ex.getFieldErrorCount());
        for (var fieldError : ex.getFieldErrors()) {
            errors.add(new ErrorExtension(fieldError.getDefaultMessage(), "invalid_" + fieldError.getField()));
        }
        return new ResponseEntity<>(new ValidationErrorResponse(errors), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<ErrorExtension> handleExpectedException(ResponseStatusException ex) {
        return new ResponseEntity<>(new ErrorExtension(ex.getReason(), ex.getStatusCode().toString()), ex.getStatusCode());
    }
}
