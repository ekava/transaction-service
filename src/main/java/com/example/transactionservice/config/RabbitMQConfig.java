package com.example.transactionservice.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Bean
    public ConnectionFactory connectionFactory() {
        return new CachingConnectionFactory("localhost"); // TODO change to variable
    }

    @Bean
    public AmqpAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        var template = new RabbitTemplate(connectionFactory());
        template.setExchange("transaction-exchange");
        return template;
    }

    @Bean
    public Queue transactionQueue() {
        return new Queue("transaction-queue");
    }

    @Bean
    public FanoutExchange transactionExchanger() {
        return new FanoutExchange("transaction-exchange");
    }

    @Bean
    public Binding transactionBindingExchange() {
        return BindingBuilder.bind(transactionQueue()).to(transactionExchanger());
    }
}
