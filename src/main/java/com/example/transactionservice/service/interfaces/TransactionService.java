package com.example.transactionservice.service.interfaces;

import com.example.transactionservice.dto.TransactionRequestDto;
import com.example.transactionservice.dto.TransactionResponseDto;

import java.util.List;
import java.util.UUID;

public interface TransactionService {
    /**
     * TODO
     * @param clientId
     * @return
     */
    List<TransactionResponseDto> getAllTransactions(UUID clientId);

    /**
     * TODO
     * @param clientId
     * @param transactionRequestDto
     */
    void buyProduct(UUID clientId, TransactionRequestDto transactionRequestDto);

    /**
     * TODO
     * @param clientId
     * @param transactionRequestDto
     */
    void sellProduct(UUID clientId, TransactionRequestDto transactionRequestDto);

    /**
     * TODO
     * @param clientId
     * @param transactionRequestDto
     */
    void fixProduct(UUID clientId, TransactionRequestDto transactionRequestDto);

    List<TransactionResponseDto> getAll();
}
