package com.example.transactionservice.service.implementations;

import com.example.transactionservice.dto.TransactionRequestDto;
import com.example.transactionservice.dto.TransactionResponseDto;
import com.example.transactionservice.entity.Transaction;
import com.example.transactionservice.entity.enums.TransactionStatus;
import com.example.transactionservice.entity.enums.TransactionType;
import com.example.transactionservice.exception.custom.BadRequestException;
import com.example.transactionservice.mapper.TransactionMapper;
import com.example.transactionservice.repository.TransactionRepository;
import com.example.transactionservice.service.interfaces.TransactionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static com.example.transactionservice.entity.enums.TransactionStatus.PROCESSING;
import static com.example.transactionservice.entity.enums.TransactionType.BUY;
import static com.example.transactionservice.entity.enums.TransactionType.FIX;
import static com.example.transactionservice.entity.enums.TransactionType.SELL;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final TransactionMapper transactionMapper;
    private final TransactionRepository transactionRepository;
    private final RabbitTemplate rabbitTemplate;
    private final ObjectMapper objectMapper;

    @Override
    @Transactional
    public List<TransactionResponseDto> getAllTransactions(UUID clientId) {
        try {
            return transactionRepository.findAllByClientId(clientId);
        } catch (DataAccessException ex) {
            throw new BadRequestException("Unable to find transactions.");
        }
    }

    @Override
    @Transactional
    public List<TransactionResponseDto> getAll() {
        try {
            return transactionRepository.findAll().stream()
                    .map(transactionMapper::transactionToTransactionResponseDto)
                    .toList();
        } catch (DataAccessException e) {
            throw new BadRequestException("Unable to find transactions.");
        }
    }

    @Override
    @Transactional
    public void buyProduct(UUID clientId, TransactionRequestDto transactionRequestDto) {
        Transaction transaction = transactionMapper.transactionRequestDtoToTransaction(transactionRequestDto);
        transaction.setClientId(clientId);
        saveProduct(transaction, BUY);


//        TODO add sending message to cafe
    }

    @Override
    @Transactional
    public void sellProduct(UUID clientId, TransactionRequestDto transactionRequestDto) {
        Transaction transaction = transactionMapper.transactionRequestDtoToTransaction(transactionRequestDto);
        transaction.setClientId(clientId);
        saveProduct(transaction, SELL);

//        TODO add sending message to cafe
    }

    @Override
    @Transactional
    public void fixProduct(UUID clientId, TransactionRequestDto transactionRequestDto) {
        Transaction transaction = transactionMapper.transactionRequestDtoToTransaction(transactionRequestDto);
        transaction.setClientId(clientId);
        saveProduct(transaction, FIX);

//        TODO add sending message to cafe
    }

    private void saveProduct(Transaction transaction, TransactionType type) {
        transaction.setType(type);
        transaction.setStatus(PROCESSING);
        transaction.setDate(LocalDateTime.now());
        transactionRepository.save(transaction);

        var sendTransactionDto = transactionMapper.transactionToTransactionResponseDto(transaction);
        try {
            var status = TransactionStatus.valueOf(rabbitTemplate.convertSendAndReceive(objectMapper.writeValueAsString(sendTransactionDto)).toString());
            transaction.setStatus(status);
            transactionRepository.save(transaction);
        } catch (JsonProcessingException ex) {
            throw new BadRequestException("Can't send message through rabbitmq.");
        }
    }
}
