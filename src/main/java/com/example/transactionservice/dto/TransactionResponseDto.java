package com.example.transactionservice.dto;

import com.example.transactionservice.entity.enums.TransactionStatus;
import com.example.transactionservice.entity.enums.TransactionType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * DTO for {@link com.example.transactionservice.entity.Transaction}
 */
public record TransactionResponseDto(UUID id,
                                     LocalDateTime date,
                                     UUID clientId,
                                     UUID itemId,
                                     BigDecimal price,
                                     TransactionType type,
                                     TransactionStatus status) implements Serializable {
}