package com.example.transactionservice.dto;

import jakarta.validation.constraints.NotNull;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * DTO for {@link com.example.transactionservice.entity.Transaction}
 */
public record TransactionRequestDto(@NotNull UUID itemId,
                                    @NotNull BigDecimal price) {
}